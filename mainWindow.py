# -*- coding: utf-8 -*-

# Code form created by: PyQt5 UI code generator 5.14.1
# Programmed by: Fuzz Mihi

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QPixmap
import numpy as np
import matplotlib.pyplot as plt
import PIL
from PIL import Image
from PIL.ImageQt import ImageQt
import io
import pyaudio

class CustomLineEdit(QtWidgets.QLineEdit):

    clicked = QtCore.pyqtSignal()

    def __init__(self,widget):
        super().__init__(widget)

    def mousePressEvent(self, QMouseEvent):
        self.clicked.emit()

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(805, 325)

        #Podaci aplikacije
        self.numOfHarmonics = 1
        self.harmonics = []
        self.harmonicsToPlay = []
        self.sound = None
        self.soundToPlay = None
        self.amplitude = 5
        self.frequency = 2000
        self.wave_length = (1.0/self.frequency)*100
        self.lock = False
        self.audio = pyaudio.PyAudio()

        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.frequency_text = CustomLineEdit(self.centralwidget)
        self.frequency_text.setGeometry(QtCore.QRect(540, 60, 231, 21))
        self.frequency_text.setObjectName("frequency_text")
        self.FreqTekst = QtWidgets.QLabel(self.centralwidget)
        self.FreqTekst.setGeometry(QtCore.QRect(420, 60, 111, 20))
        self.FreqTekst.setObjectName("FreqTekst")
        self.FreqTekst_2 = QtWidgets.QLabel(self.centralwidget)
        self.FreqTekst_2.setGeometry(QtCore.QRect(450, 30, 81, 20))
        self.FreqTekst_2.setObjectName("FreqTekst_2")
        self.amplitude_text = CustomLineEdit(self.centralwidget)
        self.amplitude_text.setGeometry(QtCore.QRect(540, 30, 231, 21))
        self.amplitude_text.setObjectName("amplitude_text")
        self.FreqTekst_3 = QtWidgets.QLabel(self.centralwidget)
        self.FreqTekst_3.setGeometry(QtCore.QRect(410, 90, 131, 20))
        self.FreqTekst_3.setObjectName("FreqTekst_3")
        self.wave_length_text = CustomLineEdit(self.centralwidget)
        self.wave_length_text.setGeometry(QtCore.QRect(540, 90, 231, 21))
        self.wave_length_text.setObjectName("wave_length_text")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(540, 150, 161, 31))
        self.pushButton.setObjectName("pushButton")
        self.HarmoniciLabel = QtWidgets.QLabel(self.centralwidget)
        self.HarmoniciLabel.setGeometry(QtCore.QRect(420, 120, 141, 20))
        self.HarmoniciLabel.setObjectName("HarmoniciLabel")
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(540, 230, 161, 31))
        self.pushButton_2.setObjectName("pushButton_2")
        self.frame = QtWidgets.QLabel(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(0, 0, 391, 231))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(540, 190, 161, 31))
        self.pushButton_3.setObjectName("pushButton_3")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 805, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        self.lock = True
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.FreqTekst.setText(_translate("MainWindow", "Frekvencija [Hz]:"))
        self.FreqTekst_2.setText(_translate("MainWindow", "Amplituda:"))
        self.FreqTekst_3.setText(_translate("MainWindow", "Valna Duljina [cm]:"))
        self.pushButton.setText(_translate("MainWindow", "Dodaj viši harmonik"))
        self.pushButton.clicked.connect(self.new_harmonic)
        self.HarmoniciLabel.setText(_translate("MainWindow", "Broj Harmonika:   "+str(self.numOfHarmonics)))
        self.pushButton_2.setText(_translate("MainWindow", "Sviraj zvuk"))
        self.pushButton_2.clicked.connect(self.play_sound)
        self.pushButton_3.setText(_translate("MainWindow", "Oduzmi viši harmonik"))
        self.pushButton_3.setEnabled(False)
        self.pushButton_3.clicked.connect(self.destroy_harmonic)
        self.frequency_text.setText(str(self.frequency))
        self.frequency_text.textChanged.connect(self.frequency_set)
        self.frequency_text.clicked.connect(self.selectAllTextf)
        self.amplitude_text.setText(str(self.amplitude))
        self.amplitude_text.textChanged.connect(self.amplitude_set)
        self.amplitude_text.clicked.connect(self.selectAllTexta)
        self.wave_length_text.setText(str(self.wave_length))
        self.wave_length_text.textChanged.connect(self.wave_length_set)
        self.wave_length_text.clicked.connect(self.selectAllTextw)
        self.lock = False
        self.set_harmonics()
        self.plot_sounds()

    def new_harmonic(self):
        _translate = QtCore.QCoreApplication.translate
        self.numOfHarmonics+=1
        self.set_harmonics()
        self.plot_sounds()
        if(self.numOfHarmonics>1):
            self.pushButton_3.setEnabled(True)
        self.HarmoniciLabel.setText(_translate("MainWindow", "Broj Harmonika:   "+str(self.numOfHarmonics)))
    
    def destroy_harmonic(self):
        _translate = QtCore.QCoreApplication.translate
        self.numOfHarmonics-=1
        self.set_harmonics()
        self.plot_sounds()
        if(self.numOfHarmonics<=1):
            self.pushButton_3.setEnabled(False)
        self.HarmoniciLabel.setText(_translate("MainWindow", "Broj Harmonika:   "+str(self.numOfHarmonics)))
    
    def play_sound(self):
        audio_stream = self.audio.open(format=pyaudio.paFloat32, channels=1, rate=44100, output=True)
        audio_stream.write(self.soundToPlay)
        audio_stream.stop_stream()
        audio_stream.close()

        _translate = QtCore.QCoreApplication.translate

    def amplitude_set(self):
        if(self.amplitude_text.text() == '' or int(self.amplitude_text.text())==0):
            self.amplitude_text.setText("1")
        _translate = QtCore.QCoreApplication.translate
        self.amplitude = int(self.amplitude_text.text())
        self.set_harmonics()
        self.plot_sounds()

    def frequency_set(self):
        if(not self.lock):
            if(self.frequency_text.text() == '' or int(self.frequency_text.text())==0):
                self.frequency_text.setText("1")
                return
            self.lock = True
            _translate = QtCore.QCoreApplication.translate
            self.frequency = float(self.frequency_text.text())
            self.wave_length = 343.0/self.frequency
            self.wave_length_text.setText(str(self.wave_length))
            self.lock = False
            self.set_harmonics()
            self.plot_sounds()

    def wave_length_set(self):
        if(not self.lock):
            if(self.wave_length_text.text() == '' or int(self.wave_length_text.text())==0):
                self.wave_length_text.setText("1")
                return
            self.lock = True
            _translate = QtCore.QCoreApplication.translate
            self.wave_length = float(self.wave_length_text.text())
            self.frequency = float(343.0/self.wave_length)
            self.frequency_text.setText(str(self.frequency))
            self.lock = False    
            self.set_harmonics()
            self.plot_sounds()

    def set_harmonics(self):
        a = self.amplitude
        x = np.arange((44100/self.frequency)*100)
        self.harmonics = []
        self.harmonicsToPlay = []
        for i in range(0,self.numOfHarmonics):
            mult = i+1
            if(i>=1):
                a=self.amplitude/4
                if(i>=4):
                    a=self.amplitude/8
            f = mult*self.frequency
            t=f/44100
            self.harmonics.append(a*np.sin(2*np.pi*t*x))
            self.harmonicsToPlay.append((a/10*np.sin(2*np.pi*np.arange(10*44100)*f/44100)).astype(np.float32))
    
    def plot_sounds(self):
        x = np.arange((44100/self.frequency)*100)
        self.sound = self.harmonics[0]
        self.soundToPlay = self.harmonicsToPlay[0]

        for h in range(1,len(self.harmonics)):
            self.sound+=self.harmonics[h]
            self.soundToPlay+=self.harmonicsToPlay[h]
        
        plt.clf()
        axes = plt.gca()
        axes.set_xlim([0,50])
        axes.set_ylim([-10,10])
        plt.plot(x, self.sound)
        buf = io.BytesIO()
        plt.savefig(buf)
        buf.seek(0)
        img = Image.open(buf)
        img = img.resize((400,250))
        qim = ImageQt(img)
        self.frame.setPixmap(QPixmap.fromImage(qim))

    def selectAllTextf(self):
        self.frequency_text.selectAll()
    
    def selectAllTexta(self):
        self.amplitude_text.selectAll()

    def selectAllTextw(self):
        self.wave_length_text.selectAll()

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
